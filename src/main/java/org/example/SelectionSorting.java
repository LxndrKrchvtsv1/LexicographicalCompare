package org.example;

import static org.example.Main.compareStrings;

public class SelectionSorting {
    public static String[] main(String[] array) {
        for (int i = 0; i < array.length; i++) {
            int pos = i;
            for (int j = i + 1; j < array.length; j++) {
                if (compareStrings(array[i], array[j]) > 0) {
                    pos = j;
                }
            }
            String temp = array[pos];
            array[pos] = array[i];
            array[i] = temp;
        }
        return array;
    }

}

package org.example;

import static org.example.Main.compareStrings;

public class BubbleSorting {

    public static String[] main(String[] array) {
        for (int i = 1; i < array.length; i++) {
            for (int j = 0; j < array.length - i; j++) {
                if (compareStrings(array[j], array[j + 1]) > 0) {
                    String temp = array[j + 1];
                    array[j + 1] = array[j];
                    array[j] = temp;
                }
            }
        }
        return array;
    }

}

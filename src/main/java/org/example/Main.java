package org.example;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        String[] array = new String[]{"Aleksandr", "Petr", "Evgeniy", "Elena", "Alena", "Anastasiya", "Polina", "Veronika", "Vladislav"};
        String[] array2 = new String[]{"Aleksandr", "Petr", "Evgeniy", "Elena", "Alena", "Anastasiya", "Polina", "Vladislav", "Veronika"};
        String[] array3 = new String[]{"Aleksandr", "Petr", "Evgeniy", "Elena", "Alena", "Anastasiya", "Polina", "Vladislav", "Veronika"};

        String[] SelectionSortedArray = SelectionSorting.main(array);
        String[] BubbleSortedArray = BubbleSorting.main(array2);
        String[] InsertionSortedArray = InsertionSorting.main(array3);

        display(SelectionSortedArray);
        display(BubbleSortedArray);
        display(InsertionSortedArray);
    }

    public static int compareStrings(String str1, String str2) {
        return str1.compareTo(str2);
    }

    public static void display(String[] array) {
        System.out.print(Arrays.toString(array) + "\n");
    }
}
package org.example;

import static org.example.Main.compareStrings;

public class InsertionSorting {
    public static String[] main(String[] array) {
        for (int i = 1; i < array.length; i++) {
            String temp = array[i];
            for (int j = i - 1; j >= 0; j--) {
                if (compareStrings(array[j], temp) > 0) {
                    array[j + 1] = array[j];
                    array[j] = temp;
                }
            }
        }
        return array;
    }
}
